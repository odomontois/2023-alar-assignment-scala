import org.scalacheck.Arbitrary
import org.scalacheck.Arbitrary.arbitrary
import org.scalacheck.Gen
import org.scalacheck.Gen
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

import Assignment.{toAsm, livenessAnalysis}
import Ast._
import Evaluator.evaluate

class Spec
  extends AnyFlatSpec
  with ScalaCheckPropertyChecks
  with should.Matchers
{
  case class CompileResult(val asm: Asm, val liveness: Map[Reg, Line])

  val testCases = Map(
    Lit(39692) -> CompileResult(Seq(Ret(Imm(39692))), Map.empty),
    Var("rubber") ->
      CompileResult(
        Seq(Load(Reg(0), "rubber"), Ret(AReg(Reg(0)))), Map(Reg(0) -> 1)
      ),
    EUnaryOp(Neg, Lit(16421)) ->
      CompileResult(
        Seq(IUnaryOp(Neg, Reg(0), Imm(16421)), Ret(AReg(Reg(0)))),
        Map(Reg(0) -> 1),
      ),
    EBinOp(Add, Var("safety"), Lit(28253)) ->
      CompileResult(
        Seq(
          Load(Reg(0), "safety"),
          IBinOp(Add, Reg(1), AReg(Reg(0)), Imm(28253)),
          Ret(AReg(Reg(1)))
        ),
        Map(Reg(0) -> 1, Reg(1) -> 2),
      ),
    EBinOp(
      Sub, EUnaryOp(Neg, Var("main")), EBinOp(Mul, Var("discovery"), Lit(64006))
    ) ->
      CompileResult(
        Seq(
          Load(Reg(0), "main"),
          IUnaryOp(Neg, Reg(1), AReg(Reg(0))),
          Load(Reg(2), "discovery"),
          IBinOp(Mul, Reg(3), AReg(Reg(2)), Imm(64006)),
          IBinOp(Sub, Reg(4), AReg(Reg(1)), AReg(Reg(3))),
          Ret(AReg(Reg(4)))
        ),
        Map(Reg(0) -> 1, Reg(1) -> 4, Reg(2) -> 3, Reg(3) -> 4, Reg(4) -> 5),
      ),
  )

  for ((expr, expectation) <- testCases) {
    it should s"compile expressions and check liveness ${expr}" in {
      val asm = toAsm(expr).toList
      asm should be (expectation.asm)
      livenessAnalysis(asm) should be (expectation.liveness)
    }
  }

  val genLit: Gen[Expr] = arbitrary[Int].map(Lit)
  val genVar: Gen[Expr] = arbitrary[String].map(Var)
  val genUnOp: Gen[Expr] = Gen.lzy(for {e <- genExpr} yield EUnaryOp(Neg, e))
  val genBinOp: Gen[Expr] =
    for {op <- Gen.oneOf(Add, Sub, Mul, Div); e0 <- genExpr; e1 <- genExpr}
    yield EBinOp(op, e0, e1)
  val genExpr: Gen[Expr] = Gen.oneOf(genLit, genVar, genUnOp, genBinOp)
  implicit val arbitraryExpr: Arbitrary[Expr] = Arbitrary(genExpr)

  "toAsm result" should "be equivalent" in {
    val hash = (s: String) => s.hashCode
    forAll { (e: Expr) =>
      evaluate(hash, e) should be (new Interpreter(hash).interpret(toAsm(e)))
    }
  }
}
