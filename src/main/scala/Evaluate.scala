import Ast._

object Evaluator {
  def evaluate(load: String => Int, e: Expr): Int =
    e match {
      case Lit(e) => e
      case Var(name) => load(name)
      case EUnaryOp(_, e) => -evaluate(load, e)
      case EBinOp(op, e0, e1) =>
        val v0 = evaluate(load, e0)
        val v1 = evaluate(load, e1)
        binops(op)(v0, v1)
    }
}
