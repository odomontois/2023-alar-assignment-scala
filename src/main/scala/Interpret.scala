import scala.annotation.tailrec

import Ast._

case class Interpreter(val loadVar: String => Int) {
  def readArg(regs: Map[Reg, Int], arg: Arg): Int =
    arg match {
      case Imm(v) => v
      case AReg(r) => regs(r)
    }

  @tailrec
  final def interpret(regs: Map[Reg, Int], asm: Asm): Int = {
    asm.toList match {
      case Nil => 0 // impossible
      case instr :: asm =>
        instr match {
          case Load(r, name) => interpret(regs + (r -> loadVar(name)), asm)
          case Ret(a) => readArg(regs, a)
          case IUnaryOp(_, r, a) =>
            interpret(regs + (r -> -readArg(regs, a)), asm)
          case IBinOp(op, r, a0, a1) =>
            interpret(
              regs + (r -> binops(op)(readArg(regs, a0), readArg(regs, a1))),
              asm,
            )
        }
    }
  }

  def interpret(asm: Asm): Int = interpret(Map.empty, asm)
}
