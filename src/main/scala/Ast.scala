object Ast {
  sealed trait UnaryOp
  final case object Neg extends UnaryOp

  sealed trait BinOp
  final case object Add extends BinOp
  final case object Sub extends BinOp
  final case object Mul extends BinOp
  final case object Div extends BinOp
  val binops =
    Map[BinOp, (Int, Int) => Int](
      Add -> (_ + _),
      Sub -> (_ - _),
      Mul -> (_ * _),
      Div -> (_ / _),
    )

  sealed trait Expr
  final case class EBinOp(oper: BinOp, e0: Expr, e1: Expr) extends Expr
  final case class EUnaryOp(oper: UnaryOp, e: Expr) extends Expr
  final case class Lit(e: Int) extends Expr
  final case class Var(name: String) extends Expr

  case class Reg(id: Int)

  sealed trait Arg
  final case class Imm(n: Int) extends Arg
  final case class AReg(r: Reg) extends Arg

  sealed trait Instruction
  final case class Load(r: Reg, varname: String) extends Instruction
  final case class IBinOp(oper: BinOp, r: Reg, a0: Arg, a1: Arg)
    extends Instruction
  final case class IUnaryOp(oper: UnaryOp, r: Reg, a: Arg) extends Instruction
  final case class Ret(a: Arg) extends Instruction

  type Asm = Seq[Instruction]

  type Line = Int
}
