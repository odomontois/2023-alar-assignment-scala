import cats.data.RWS
import cats.data.RWS.{get, modify, pure, tell}
import cats.kernel.instances.VectorMonoid

import Ast._

object CompilerImperativeStyle {
  def compile(e: Expr): Asm = {
    var registerCounter: Int = 0
    var asm: Asm = Seq.empty

    def freshReg(): Reg = {
      val r = registerCounter
      registerCounter += 1
      Reg(r)
    }

    def go(e: Expr): Arg =
      e match {
        case Lit(e) => Imm(e)
        case Var(name) =>
          val r = freshReg()
          asm = asm :+ Load(r, name)
          AReg(r)
        case EUnaryOp(op, e0) =>
          val r0 = go(e0)
          val r = freshReg()
          asm = asm :+ IUnaryOp(op, r, r0)
          AReg(r)
        case EBinOp(op, e0, e1) =>
          val r0 = go(e0)
          val r1 = go(e1)
          val r = freshReg()
          asm = asm :+ IBinOp(op, r, r0, r1)
          AReg(r)
      }

    val res = go(e)
    asm :+ Ret(res)
  }
}

object CompilerFunctionalStyle {
  private implicit val vectorMonoid = new VectorMonoid[Instruction]()

  private val freshReg: RWS[Unit, Vector[Instruction], Int, Reg] =
    for {
      r <- get; _ <- modify[Unit, Vector[Instruction], Int](_ + 1)
    } yield Reg(r)

  private def tell1(instr: Instruction) =
    tell[Unit, Vector[Instruction], Int](Vector(instr))

  private def go(e: Expr): RWS[Unit, Vector[Instruction], Int, Arg] =
    e match {
      case Lit(e) => pure(Imm(e))
      case Var(name) =>
        for { r <- freshReg; _ <- tell1(Load(r, name)) } yield AReg(r)
      case EUnaryOp(op, e0) =>
        for {
          r0 <- go(e0); r <- freshReg; _ <- tell1(IUnaryOp(op, r, r0))
        } yield AReg(r)
      case EBinOp(op, e0, e1) =>
        for {
          r0 <- go(e0)
          r1 <- go(e1)
          r <- freshReg
          _ <- tell1(IBinOp(op, r, r0, r1))
        } yield AReg(r)
    }

  def compile(e: Expr): Asm = {
    val (asm, _, r) = go(e).run((), 0).value
    asm :+ Ret(r)
  }
}

object Assignment {
  // Convert to ASM
  def toAsm(e: Expr): Asm =
    CompilerFunctionalStyle.compile(e)
  // CompilerImperativeStyle.compile(e)

  private def registersUsed(arg: Arg): Seq[Reg] =
    arg match {
      case AReg(r) => Seq(r)
      case _       => Seq.empty
    }

  private def registersUsed(instr: Instruction): Seq[Reg] =
    instr match {
      case Ret(a)               => registersUsed(a)
      case IUnaryOp(_, _, a)    => registersUsed(a)
      case IBinOp(_, _, a0, a1) => registersUsed(a0) ++ registersUsed(a1)
      case _                    => Seq.empty
    }

  // Liveness -- the last occurrence of each register
  def livenessAnalysis(program: Asm): Map[Reg, Line] =
    program.view.zipWithIndex.flatMap { case (instr, line) =>
      registersUsed(instr).map(_ -> line)
    }.toMap
}
